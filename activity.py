# Specifications
# 1. Create 5 variables and output them in the command prompt in the following format:
# "I am <name (string)>, and I am <age (integer)> years old, I work as a <occupation (string)>, and my rating for <movie (string)> is <rating (decimal)>"

name = "Malthael"
age = 120
occupation = "Mage"
movie = "Devil's Advocate"
rating = 99.7

print(f"I am {name}, and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {rating}")


# 2. Perform the following statements
# Create 3 variables, num1, num2, num3
num1 = 9
num2 = 15
num3 = 12

# Get the product of num1 and num2
print(num1 * num2)

# Check if num1 is less than num3
print(num1 < num3)

# Add the value of num3 to num2
num2 += num3
print(num2)
